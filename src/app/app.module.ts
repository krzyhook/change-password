import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { ChangePasswordComponent } from './change-password/change-password.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { ControlMessagesComponent } from './control-messages.component';
import { ValidationService } from './validation.service';
import { NewPasswordComponent } from './new-password/new-password.component';
import { RecoveryNewPasswordComponent } from './recovery-new-password/recovery-new-password.component';
import { RegisterComponent } from './register/register.component';


@NgModule({
  declarations: [
    AppComponent,
    ChangePasswordComponent,
    ControlMessagesComponent,
    NewPasswordComponent,
    RecoveryNewPasswordComponent,
    RegisterComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    FormsModule
  ],
  providers: [ValidationService],
  bootstrap: [AppComponent]
})
export class AppModule { }
