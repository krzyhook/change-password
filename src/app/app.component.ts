import { Component } from '@angular/core';

@Component({
  selector: 'khk-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'Password validations';
}
