import { Component, Input, AfterViewInit } from '@angular/core';
import { FormGroup, Validators, AbstractControl } from '@angular/forms';

@Component({
  selector: 'khk-new-password',
  templateUrl: './new-password.component.html'
})
export class NewPasswordComponent implements AfterViewInit {
  @Input() passwordText: string;
  @Input() passwordConfirmText: string;
  @Input() passwordGroup: FormGroup;

  public get password(): AbstractControl { return this.passwordGroup.controls.password; }
  public get passwordConfirm(): AbstractControl { return this.passwordGroup.controls.passwordConfirm; }

  ngAfterViewInit(): void {
    this.password.setValidators([
      Validators.required,
      Validators.pattern(/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9#?!@$%^&*_\-]).{8,}$/)
    ]);
    this.passwordConfirm.setValidators([
      Validators.required
    ]);
    this.passwordGroup.setValidators(this.passwordMatchValidator);
  }

  private passwordMatchValidator(formGroup: FormGroup): { [key: string]: boolean } | null {
    const password = formGroup.get('password');
    const passwordConfirm = formGroup.get('passwordConfirm');

    return password.value !== passwordConfirm.value ? { 'passwordMismatch': true } : null;
  }
}
