import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'khk-register',
  templateUrl: './register.component.html'
})
export class RegisterComponent implements OnInit {
  registerForm: FormGroup;
  errors: string;
  isSubmitting: boolean;

  constructor(
    private fb: FormBuilder,
    private router: Router) {
      this.isSubmitting = false;
  }

  get nickname() { return this.registerForm.get('nickname'); }
  get email() { return this.registerForm.get('email'); }
  get password() { return this.registerForm.get('newPassword.password'); }
  get confirmPassword() { return this.registerForm.get('newPassword.confirmPassword'); }

  ngOnInit() {
    this.registerForm = this.fb.group({
      nickname:         ['', [Validators.required]],
      email:            ['', [Validators.required, Validators.email]],
      newPassword: this.fb.group({
        password: '',
        passwordConfirm: ''
      })
    });
  }

  onSubmit($event: Event) {
    this.isSubmitting = true;
    if (this.registerForm.valid) {
      console.log(this.registerForm.value);
    }
  }

  private handleError(err: any): void {
    this.errors = err.error.Message || 'Registration failed';
  }
}
