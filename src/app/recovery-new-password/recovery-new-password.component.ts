import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Params } from '@angular/router';

@Component({
    selector: 'khk-recovery-new-password',
    templateUrl: './recovery-new-password.component.html'
})
export class RecoveryNewPasswordComponent implements OnInit {
    newPasswordForm: FormGroup;
    userId: number;
    token: string;

    constructor(private fb: FormBuilder) {
    }

    get newPassword() { return this.newPasswordForm.get('password'); }

    ngOnInit() {
        this.createForm();
        this.userId = 123;
        this.token = 'ABCDEF';
    }

    onSubmit($event: Event) {
        if (this.newPasswordForm.valid) {
            console.log(this.userId, this.token, this.newPassword.value);
        }
    }

    private createForm(): void {
        this.newPasswordForm = this.fb.group({
          password: '',
          passwordConfirm: ''
        });
    }
}
