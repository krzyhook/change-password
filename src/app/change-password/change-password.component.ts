import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'khk-change-password',
  templateUrl: './change-password.component.html'
})
export class ChangePasswordComponent implements OnInit {
  public changePasswordForm: FormGroup;
  public errors: string;

  constructor(private fb: FormBuilder) {
  }

  public get oldPassword() { return this.changePasswordForm.get('oldPassword'); }
  private get newPassword() { return this.changePasswordForm.get('newPassword.password'); }

  ngOnInit() {
    this.createForm();
  }

  public onSubmit($event: Event): void {
    if (this.changePasswordForm.valid) {
      console.log(this.oldPassword.value, this.newPassword.value);
    }
  }

  private createForm(): void {
    this.changePasswordForm = this.fb.group({
      oldPassword: ['', [Validators.required]],
      newPassword: this.fb.group({
        password: '',
        passwordConfirm: ''
      })
    });
  }

  private handleError(err: any): void {
    switch (err.status) {
      case 401: {
         this.errors = 'Unauthorized. You must be logged-in.';
         break;
      }
      default: {
        this.errors = err.error.Message || 'Change password failed';
      }
    }
  }
}
