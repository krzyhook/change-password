﻿import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, AbstractControl, ValidatorFn, FormArray } from '@angular/forms';
import { Router } from '@angular/router';

import { MatSnackBar, MatSnackBarConfig } from '@angular/material';

import { AccountChangePassword } from '../shared/models/account.changepassword';
import { UserService } from '../shared/services/user.service';

import { CookieModule, CookieService } from 'ngx-cookie';
import { CommonService } from '../shared/utils/common.service';

import { ParentErrorStateMatcher } from '../shared/utils/parenterrorstatematcher.service';

/** cross field validation */
function passwordMatcher(c: AbstractControl): { [key: string]: boolean } | null {
    const passwordControl = c.get('newPassword');
    const confirmControl = c.get('confirmPassword');

    if (passwordControl.pristine || confirmControl.pristine) {
        return null;
    }

    if (passwordControl.value === confirmControl.value) {
        return null;
    }
    return { 'match': true };
}

@Component({
    selector: 'app-changepassword-form',
    templateUrl: '../account/account-changepassword.component.html'
})

export class ChangePasswordComponent implements OnInit {

    changePasswordForm: FormGroup; /** root form group */

    /** handling error messages in the code rather than front end */
    newPasswordMessage: string;

    errors: string;
    isRequesting: boolean;

    parentErrorStateMatcher = new ParentErrorStateMatcher();

    color = 'primary';
    mode = 'indeterminate';
    spinnerValue = 0;

    private isLocalStorageAvailable = false;

    constructor(private userService: UserService, private router: Router, private fb: FormBuilder, public snackBar: MatSnackBar,
        private commonService: CommonService, private cookieService: CookieService) {
        //this.isLocalStorageAvailable = false;
        this.isLocalStorageAvailable = commonService.isLocalStorageAvailable();
    }

    ngOnInit() {
        /** formBuilder creates a form model from configuration */
        this.changePasswordForm = this.fb.group({
            /** form controls, key value pairs */
            /** can also be array */
            /** 1st array element is default value, 2nd is array of validation rules, 3rd is async validators */
            /** eg server side validation, these run after synchronous validators pass */
            currentPassword: ['', Validators.required],
            /** lastName: { value: 'n/a', disabled: true }, to disable/enable form control */
            /** nested form group for cross field validation */
            newPasswordGroup: this.fb.group({
                newPassword: ['', [Validators.required, Validators.pattern('((?=.*[0-9])(?=.*[a-z])(?=.*[A-Z]).{6,20})')]],
                confirmPassword: ['', [Validators.required, Validators.pattern('((?=.*[0-9])(?=.*[a-z])(?=.*[A-Z]).{6,20})')]]
            }, { validator: passwordMatcher })
        });
    }

    changePassword() {
        this.isRequesting = true;
        this.errors = '';

        const currentPasswordControl = this.changePasswordForm.get('currentPassword');
        const newPasswordControl = this.changePasswordForm.get('newPasswordGroup.newPassword');

        this.userService.changePassword(currentPasswordControl.value, newPasswordControl.value)
            .finally(() => this.isRequesting = false)
            .subscribe(
            result => {
                if (result) {
                    //log user out
                    this.userService.logout();
                    let config = new MatSnackBarConfig();
                    config.politeness = 'assertive';
                    config.duration = 4000;
                    config.extraClasses = ['snack-bar-success'];

                    if (this.isLocalStorageAvailable) {
                        if (localStorage.getItem('selectedLang') === 'fi') {
                            this.snackBar.open('Salasanasi on vaihdettu! Sisäänkirjaudu uudella salasanallasi', '', config);
                        }
                        else {
                            this.snackBar.open('Password changed! Please login with your new password', '', config);
                        }
                    }
                    else {
                        //use cookies
                        if (this.cookieService.get('selectedLang') === 'fi') {
                            this.snackBar.open('Salasanasi on vaihdettu! Sisäänkirjaudu uudella salasanallasi', '', config);
                        }
                        else {
                            this.snackBar.open('Password changed! Please login with your new password', '', config);
                        }
                    }
                    
                    this.router.navigate(['/Login']);
                    //this.router.navigate(['/Login'], { queryParams: { redirectFrom: 'changepassword', email: '' } });
                }
            },
            errors => {
                this.errors = errors;

                let config = new MatSnackBarConfig();
                config.politeness = 'assertive';
                config.duration = 4000;
                config.extraClasses = ['snack-bar-warning'];
                let snackBarRef = this.snackBar.open(this.errors, '', config);
            });
    }
}
