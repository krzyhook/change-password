import { Component, OnInit } from '@angular/core';
import {
  FormControl,
  FormGroup,
  FormBuilder,
  Validators,
  AbstractControl,
  ValidatorFn
} from '@angular/forms';

@Component({
  selector: 'khk-change-password-old',
  templateUrl: './change-password.component.html'
})
export class ChangePasswordComponent implements OnInit {
  changePasswordForm: FormGroup;

  private formErrors = {
    'oldPassword': '',
    'newPassword': '',
    'newPasswordConfirmed': '',
  };
  private validationMessages = {
    'oldPassword': {
      'required': 'Field is required'
    },
    'newPassword': {
      'required': 'required',
      'pattern': 'pattern'
    },
    'newPasswordConfirmed': {
      'mismatch': 'Password mismatch'
    }
  };

  constructor(private fb: FormBuilder) {
    this.createForm();
  }

  ngOnInit() {
    // this.changePasswordForm.valueChanges.subscribe(data => this.onValueChanged(data));
    //// this.changePasswordForm.statusChanges.subscribe(data => this.onValueChanged(data));
  }

  onSubmit($event: Event) {
    if (this.changePasswordForm.valid) {
      console.log('FORM VALID');
    } else {
      this.onValueChanged(null, false);
      console.error('INVALID FORM');
    }
    $event.preventDefault();
    return false;
  }

  onValueChanged(data?: any, dirtyOnly = true) {
    console.log('np', this.newPassword, data);
    // console.log('ddd', data);
    if (!this.changePasswordForm) { return; }
    const form = this.changePasswordForm;

    for (const field in this.formErrors) {
      if (this.formErrors.hasOwnProperty(field)) {
        // clear previous error message (if any)
        this.formErrors[field] = '';
        const control = form.get(field);
        const strLog = `valid:${control.valid},dirty:${control.dirty},pristine:${control.pristine},touched:${control.touched}`
         console.log(field, strLog, control, data);
        if (control && !control.valid) {
          const messages = this.validationMessages[field];
          for (const key in control.errors) {
            if (control.errors.hasOwnProperty(key)) {
              if (messages[key] && (dirtyOnly === false || (dirtyOnly === true && control.dirty))) {
                  this.formErrors[field] = messages[key];
              }
            }
          }
        }
      }
    }
  }

  get oldPassword() { return this.changePasswordForm.get('oldPassword'); }
  get newPassword() { return this.changePasswordForm.get('newPassword'); }
  get newPasswordConfirmed() { return this.changePasswordForm.get('newPasswordConfirmed'); }

  private createForm() {
    this.changePasswordForm = this.fb.group({
      oldPassword: ['',
                    // { updateOn: 'blur' },
                    [ Validators.required ]
                   ],
      newPassword: ['',
                    // { updateOn: 'blur' },
                    [ Validators.required, Validators.pattern(/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9#?!@$%^&*_\-]).{8,}$/) ]
                   ],
      newPasswordConfirmed: ['',
                            //  { updateOn: 'blur' },
                             [ Validators.required ]
                            ]
    }, { validator: this.passwordMatchValidator });
  }

  passwordMatchValidator(g: FormGroup): { [key: string]: boolean } | null {
    const password = g.get('newPassword');
    const passwordConfirm = g.get('newPasswordConfirmed');

    if (password.pristine || passwordConfirm.pristine) {
      return null;
    }

    if (password.value === passwordConfirm.value) {
      return null;
    }
    const error = { 'mismatch': true };
    passwordConfirm.setErrors(error);

    return error;
  }
}
